package nl.utwente.di.Calculator;


public class Calculator {

    public double calculate(double celsius) {
        return (celsius * ((double) 9/5) + 32);
    }
}
